FROM debian:stable-slim
COPY --from=msoap/shell2http /app/shell2http /bin

# Configure apt
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
      zenity \
      pass \
      curl \
      gnupg

ARG AWS_VAULT_VERSION
ENV AWS_VAULT_PROMPT=zenity
# Install aws-vault tool
RUN curl -L https://github.com/99designs/aws-vault/releases/download/v${AWS_VAULT_VERSION}/aws-vault-linux-amd64 -o /bin/aws-vault && chmod +x /bin/aws-vault

ENTRYPOINT ["/bin/shell2http"]

CMD ["-help"]